from datetime import datetime
from flask import url_for
from ecosynthdata.exceptions import ValidationError
import bleach
import markdown
from . import db


class Post(db.Model):
    __tablename__ = 'posts'
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    title = db.Column(db.Text)
    body = db.Column(db.Text)
    body_html = db.Column(db.Text)

    def __repr__(self):
        return '<Post %r>' % self.title

    @staticmethod
    def generate_fake(count=100):
        from random import seed, randint
        import forgery_py

        seed()
        for i in range(count):
            p = Post(timestamp=forgery_py.date.date(True),
                     title=forgery_py.lorem_ipsum.title(1),
                     body=forgery_py.lorem_ipsum.sentences(randint(1, 5)))
            db.session.add(p)
            db.session.commit()

    @staticmethod
    def on_changed_body(target, value, oldvalue, initiator):
        allowed_tags = ['a', 'abbr', 'acronym', 'b', 'blockquote', 'code',
                        'em', 'i', 'li', 'ol', 'pre', 'strong', 'ul',
                        'h1', 'h2', 'h3', 'p']
        target.body_html = bleach.linkify(bleach.clean(
            markdown(value, output_format='html'),
            tags=allowed_tags, strip=True))

    def to_json(self):
        json_post = {
            'url': url_for('api.get_post', id=self.id, _external=True),
            'body': self.body,
            'body_html': self.body_html,
        }
        return json_post

    @staticmethod
    def from_json(json_post):
        body = json_post.get('body')
        if body is None or body == '':
            raise ValidationError('post does not have a body')
        return Post(body=body)


#db.event.listen(Post.body, 'set', Post.on_changed_body)


class SynthSet(db.Model):
    __tablename__ = 'synthsets'
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    ov_author = db.Column(db.Text)
    ov_organization = db.Column(db.Text)
    ov_doi = db.Column(db.Text)
    acq_site = db.Column(db.Text)
    acq_lat = db.Column(db.Float)
    acq_lon = db.Column(db.Float)
    acq_date = db.Column(db.Date)
    acq_time = db.Column(db.Time)
    gen_cv_type = db.Column(db.Text)
    gen_cv_version = db.Column(db.Text)
    notes = db.Column(db.Text)
    img_filepath = db.Column(db.Text)
    ply_filepath = db.Column(db.Text)
    out_filepath = db.Column(db.Text)
    ecobrowser_ply_filepath = db.Column(db.Text)
    viewable_cloud = db.Column(db.Boolean)

    def __repr__(self):
        return '<SynthSet %r>' % self.id

    @staticmethod
    def generate_fake(count=20):
        import datetime
        from faker import Factory
        faker = Factory.create()

        for i in range(count):
            s = SynthSet(
                timestamp=faker.date_time(),
                ov_author=faker.name(),
                ov_organization=faker.company(),
                ov_doi="http://dx.doi.org/10.5072/FK2KH0H305",
                acq_site=faker.city(),
                acq_lat=faker.latitude(),
                acq_lon=faker.longitude(),
                acq_date=datetime.date(2013,12,24),
                acq_time=datetime.time(13,01),
                gen_cv_type="Ecosynther",
                gen_cv_version="0.8",
                notes=faker.paragraph(),
                img_filepath=faker.uri_path(),
                ply_filepath=faker.uri_path(),
                out_filepath=faker.uri_path(),
                ecobrowser_ply_filepath=faker.uri_path(),
                viewable_cloud=False
                )

            db.session.add(s)
            db.session.commit()

    def to_json(self):

        json_post = {
            'id': self.id,
            'timestamp': self.timestamp,
            'ov_author': self.ov_author,
            'ov_organization': self.ov_organization,
            'ov_doi': self.ov_doi,
            'acq_site': self.acq_site,
            'acq_lat': self.acq_lat,
            'acq_lon': self.acq_lon,
            'acq_date': str(self.acq_date),
            'acq_time': str(self.acq_time),
            'gen_cv_type': self.gen_cv_type,
            'gen_cv_version': self.gen_cv_version,
            'img_filepath': self.img_filepath,
            'ply_filepath': self.ply_filepath,
            'out_filepath': self.out_filepath,
            'ecobrowser_ply_filepath': self.ecobrowser_ply_filepath,
            'viewable_cloud': self.viewable_cloud,
            'notes': self.notes
        }
        return json_post

"""
class User(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    nickname = db.Column(db.String(64), unique=True)
    email = db.Column(db.String(120), unique=True)
    role = db.Column(db.SmallInteger)
    posts = db.relationship('Post', backref='author', lazy='dynamic')

    def __init__(self, username, password, email):
        self.username = username
        self.password = password
        self.email = email
        self.registered_on = datetime.utcnow()

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def __repr__(self):
        return '<User %r>' % (self.username)
"""
