from flask import jsonify, request, url_for, current_app
from .. import db
from ..models import SynthSet
from . import api
from functools import wraps


def support_jsonp(func):
    """Wraps JSONified output for JSONP requests."""
    @wraps(func)
    def decorated_function(*args, **kwargs):
        callback = request.args.get('callback', False)
        if True:  # not callback:
            data = str(func(*args, **kwargs).data)
            content = 'eqfeed_callback(' + data + ')'  # str(callback) +
            mimetype = 'application/javascript'
            return current_app.response_class(content, mimetype=mimetype)
        else:
            return func(*args, **kwargs)
    return decorated_function


@api.route('/synthsets/')
def get_synths():
    page = request.args.get('page', 1, type=int)
    pagination = SynthSet.query.paginate(
        page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
        error_out=False)
    synthsets = pagination.items
    prev = None
    if pagination.has_prev:
        prev = url_for('api.get_synths', page=page-1, _external=True)
    next = None
    if pagination.has_next:
        next = url_for('api.get_synths', page=page+1, _external=True)
    return jsonify({
        'synthsets': [synthset.to_json() for synthset in synthsets],
        'prev': prev,
        'next': next,
        'count': pagination.total
    })


@api.route('/synthsets/<int:id>')
def get_synth(id):
    synthset = SynthSet.query.get_or_404(id)
    return jsonify(synthset.to_json())


@api.route('/synthsets/', methods=['POST'])
def new_synth():
    synthset = SynthSet.from_json(request.json)
    db.session.add(synthset)
    db.session.commit()
    return jsonify(synthset.to_json()), 201, \
        {'Location': url_for('api.get_synth', id=synthset.id, _external=True)}


@api.route('/synthsets/<int:id>', methods=['PUT'])
def edit_synth(id):
    synthset = SynthSet.query.get_or_404(id)
    synthset.body = request.json.get('body', synthset.body)
    db.session.add(synthset)
    return jsonify(synthset.to_json())


@api.route('/synthsets-all-geo/')
@support_jsonp
def get_synthsets_all_geo():
    synthsets = SynthSet.query.all()
    return jsonify({
        'synthsets': [synthset.to_json() for synthset in synthsets],
        'count': len(synthsets)
    })


@api.route('/synthsets-geo/<int:id>')
@support_jsonp
def get_synthset_geo(id):
    synthset = SynthSet.query.get_or_404(id)
    return jsonify({
        'synthsets': [synthset.to_json()],
        'count': 1
    })
