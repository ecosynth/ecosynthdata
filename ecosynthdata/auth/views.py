from flask import render_template, redirect, url_for, request, current_app,\
    send_from_directory
from werkzeug import secure_filename
#from .forms import 
from ..models import Post, SynthSet, User
from .. import db
from . import auth

import os


@auth.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'GET':
        return render_template('register.html')
    user = User(
        request.form['username'],
        request.form['password'],
        request.form['email']
        )
    db.session.add(user)
    db.session.commit()
    flash('User successfully registered')
    return redirect(url_for('login'))
 
@auth.route('/login',methods=['GET','POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    return redirect(url_for('index'))