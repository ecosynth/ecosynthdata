from flask import render_template, redirect, url_for, request, current_app,\
    send_from_directory
from werkzeug import secure_filename
from .forms import PostForm, SynthSetForm
from ..models import Post, SynthSet
from .. import db
from . import main
from .doi import create_doi
from .publish_ecobrowser import publish_ecobrowser

import os


@main.route("/test2")
def test2():
    return render_template('site/test2.html')


@main.route("/test")
def test():
    return render_template('site/test.html')


@main.route("/")
def index():
    return render_template('site/index.html')


@main.route("/map")
def map():
    return render_template('site/map.html')


@main.route("/explore")
def explore():
    page = request.args.get('page', 1, type=int)
    query = SynthSet.query
    pagination = query.order_by(SynthSet.timestamp.desc()).paginate(
        page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
        error_out=True)
    synthsets = pagination.items
    return render_template('site/explore.html', synthsets=synthsets,
                           pagination=pagination)


@main.route("/explore-site")
def explore_site():
    page = request.args.get('page', 1, type=int)
    query = SynthSet.query
    pagination = query.order_by(SynthSet.acq_site.asc()).paginate(
        page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
        error_out=True)
    synthsets = pagination.items
    return render_template('site/explore_site.html', synthsets=synthsets,
                           pagination=pagination)


@main.route("/explore-author")
def explore_author():
    page = request.args.get('page', 1, type=int)
    query = SynthSet.query
    pagination = query.order_by(SynthSet.ov_author.asc()).paginate(
        page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
        error_out=True)
    synthsets = pagination.items
    return render_template('site/explore_author.html', synthsets=synthsets,
                           pagination=pagination)


@main.route("/explore-org")
def explore_org():
    page = request.args.get('page', 1, type=int)
    query = SynthSet.query
    pagination = query.order_by(SynthSet.ov_organization.asc()).paginate(
        page, per_page=current_app.config['FLASKY_POSTS_PER_PAGE'],
        error_out=True)
    synthsets = pagination.items
    return render_template('site/explore_org.html', synthsets=synthsets,
                           pagination=pagination)


@main.route("/search")
def search():
    return render_template('site/search.html')


@main.route("/create", methods=['GET', 'POST'])
def create():
    form = SynthSetForm()
    if form.validate_on_submit():
        if form.upload_key and (
            form.upload_key.data == current_app.config['UPLOAD_KEY']):

            # Warning: This Operation is Not Atomic, but Should Be Atomic
            query = SynthSet.query

            try:
                post_id = query.order_by(SynthSet.id.desc()).first().id + 1
            except:
                post_id = 1

            # Make directory for new data
            filepath = os.path.join(
                current_app.config['UPLOAD_FOLDER'], str(post_id))

            if not os.path.exists(filepath):
                os.mkdir(filepath)

            # Add files
            img_filepath = None
            if form.img_zip and allowed_file(form.img_zip.data.filename):
                img_filename = secure_filename(form.img_zip.data.filename)
                form.img_zip.data.save(os.path.join(filepath, img_filename))
                img_filepath = os.path.join(str(post_id), img_filename)

            ply_filepath = None
            if form.ply and allowed_file(form.ply.data.filename):
                ply_filename = secure_filename(form.ply.data.filename)
                form.ply.data.save(os.path.join(filepath, ply_filename))
                ply_filepath = os.path.join(str(post_id), ply_filename)

            viewable_cloud = False
            ecobrowser_ply_filepath = None
            if form.ecobrowser_ply and allowed_file(form.ecobrowser_ply.data.filename):
                ecobrowser_ply_filename = secure_filename(
                    form.ecobrowser_ply.data.filename)
                form.ecobrowser_ply.data.save(
                    os.path.join(filepath, ecobrowser_ply_filename))
                ecobrowser_ply_filepath = os.path.join(
                    str(post_id), ecobrowser_ply_filename)

                # Process EcoBrowser PLY
                success = publish_ecobrowser(
                    os.path.join(filepath, ecobrowser_ply_filename), str(post_id))

                # Check for successful processing
                if success:
                    viewable_cloud = True

            out_filepath = None
            if form.out and allowed_file(form.out.data.filename):
                out_filename = secure_filename(form.out.data.filename)
                form.out.data.save(os.path.join(filepath, out_filename))
                out_filepath = os.path.join(str(post_id), out_filename)

            ov_doi = create_doi(
                current_app.config['EZID_USERNAME'],
                current_app.config['EZID_PASSWORD'],
                current_app.config['EZID_SHOULDER'],
                form.ov_author.data,
                "Ecosynth",
                form.acq_site.data,
                form.acq_date.data,
                form.acq_time.data
                )

           # Add post
            synthset = SynthSet(
                ov_author=form.ov_author.data,
                ov_organization=form.ov_organization.data,
                ov_doi=ov_doi,
                acq_site=form.acq_site.data,
                acq_lat=form.acq_lat.data,
                acq_lon=form.acq_lon.data,
                acq_date=form.acq_date.data,
                acq_time=form.acq_time.data,
                gen_cv_type=form.gen_cv_type.data,
                gen_cv_version=form.gen_cv_version.data,
                img_filepath=img_filepath,
                ply_filepath=ply_filepath,
                out_filepath=out_filepath,
                ecobrowser_ply_filepath=ecobrowser_ply_filepath,
                viewable_cloud=viewable_cloud,
                notes=form.notes.data
                )

            db.session.add(synthset)

            return redirect(url_for('.synthset', id=post_id))

        else:
            return render_template('401.html')

    return render_template('synthset/new_synthset.html', form=form)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in \
           current_app.config['ALLOWED_EXTENSIONS']


@main.route('/explore/<int:id>', methods=['GET'])
def synthset(id):
    synthset = SynthSet.query.get_or_404(id)
    return render_template(
        'synthset/synthset.html', synthset=[synthset])


@main.route('/view/<int:id>')
def viewer(id):
    #return send_from_directory(current_app.static_folder, 'Viewer/')
    path = 'http://%s/?table=%i' % (current_app.config['VIEWER_LOCATION'], id) 
    return redirect(path)
    #return render_template('synthset/view_synth.html', synthset=str(id))


@main.route('/uploads/<path:filepath>')
def uploaded_file(filepath):
    filepath = os.path.join(current_app.config['UPLOAD_FOLDER'], filepath)
    dirname = os.path.dirname(filepath)
    #print "dirname: ", dirname
    basename = os.path.basename(filepath)
    #print "basename: ", basename
    return send_from_directory(dirname, basename)


@main.route('/newpost', methods=['GET', 'POST'])
def newpost():
    form = PostForm()
    if form.validate_on_submit():
        post = Post(title=form.title.data,
                    body=form.body.data)
        db.session.add(post)
        return redirect(url_for('.posts'))
    return render_template('post/new_post.html', form=form)


@main.route('/post/<int:id>', methods=['GET'])
def post(id):
    post = Post.query.get_or_404(id)
    return render_template('post/post.html', posts=[post])


@main.route("/hello")
def hello():
    return "Hello World!"
