import subprocess
import os

from flask import current_app


def publish_ecobrowser(ecobrowser_ply_filepath, post_id):
    """
    Uploads ecobrowser point cloud to MySQL database so that it
    can be viewed through the web

    :param file ecobrowser_ply_filepath:
    :param string post_id:

    :return: success_signal
    :rtype: boolean

    NOTE: Code in basedir/ecobrowser_code must be compiled before
        this can be used

        Still need to properly link EcosynthData to Viewer
    """
    try:
        proc = subprocess.call([
            # args list
            os.path.join(
                current_app.config['BASEDIR'],
                "browser_processing/BuildQT/BuildQuadTree"
                ),
            ecobrowser_ply_filepath,
            str(post_id)
            ], stdout=subprocess.PIPE)

        #print "Proc1:", proc

        if proc != 0:
            raise Exception

        proc = subprocess.call([
            # args list
            os.path.join(
                current_app.config['BASEDIR'],
                "browser_processing/BuildOct/BuildOctree"
                ),
            ecobrowser_ply_filepath,
            str(post_id)
            ], stdout=subprocess.PIPE)

        #print "Proc2:", proc

        if proc != 0:
            raise Exception

        # Generate Map HERE

        return True

    except:
        return False

#if __name__ == "__main__":

#    ecobrowser_ply_filepath = "/vagrant/ecosynthdata/uploads/53/ecobrowser_ready.ply"
#    post_id = str(5)
#    publish_ecobrowser(ecobrowser_ply_filepath, post_id)
