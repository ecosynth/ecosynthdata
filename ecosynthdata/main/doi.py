import subprocess
import datetime
import os

dirname = os.path.dirname(os.path.realpath(__file__))
ezid_filepath = os.path.join(dirname, "ezid.py")


def create_doi(username, password, shoulder, creator, publisher, site, acq_date, acq_time):
    """
    This function works with ezid.py to create a basic DOI for a SynthSet on
    EcosynthData
    """
    try:
        proc = subprocess.Popen([
            "python", ezid_filepath, (username + ":" + password), "mint", shoulder,
            "datacite.creator", creator,
            "datacite.title", ("SynthSet for " + site + " Acquired " + str(acq_date) + " " + str(acq_time)),
            "datacite.publisher", publisher,
            "datacite.publicationyear", str(datetime.date.today().year),
            "datacite.resourcetype", "Dataset"
            ], stdout=subprocess.PIPE)
        lines = proc.stdout.readlines()

        if lines[0].split()[0].startswith("success:"):
            return lines[0].split()[1]

        else:
            return None

    except:
        pass
