from flask.ext.wtf import Form
from wtforms import StringField, TextAreaField, BooleanField, SelectField,\
    SubmitField, DateField, FileField, FloatField
from wtforms_components import TimeField
from wtforms.validators import Required, Length, Email, Regexp
from wtforms import ValidationError


class PostForm(Form):
    title = StringField("Title:", validators=[Required()])
    body = TextAreaField("Post:", validators=[Required()])
    submit = SubmitField('Submit')


class SynthSetForm(Form):
    ov_author = StringField("Author:", validators=[Required()])
    ov_organization = StringField("Organization:")
    acq_site = StringField("Acquisition Site:", validators=[Required()])
    acq_lat = FloatField("Latitude:")
    acq_lon = FloatField("Longitude:")
    acq_date = DateField("Acquisition Date (YYYY-MM-DD):")
    acq_time = TimeField("Acquisition Time (e.g. 12:00:PM):")
    gen_cv_type = StringField("SfM Program Type:")
    gen_cv_version = StringField("SfM Program Version:")
    img_zip = FileField('Images (*.zip) (Optional)')
    ply = FileField('Point Cloud PLY File (*.ply) (Optional)')
    ecobrowser_ply = FileField('EcoBrowser-Ready PLY File (*.ply) (Optional)')
    out = FileField('Bundler OUT File (*.out) (Optional)')
    notes = TextAreaField("Notes:")
    upload_key = StringField("Upload Key (Contact Ecosynth for Key):")
    submit = SubmitField('Submit')
