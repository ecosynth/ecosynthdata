import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'hard to guess string'
    SSL_DISABLE = False
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_RECORD_QUERIES = True
    FLASKY_POSTS_PER_PAGE = 15
    FLASKY_SLOW_DB_QUERY_TIME = 0.5
    UPLOAD_FOLDER = os.path.join(basedir, 'uploads')
    ALLOWED_EXTENSIONS = set(['zip', 'ply', 'out', 'log'])
    EZID_USERNAME = 'FILLIN'  #os.environ.get('EZID_USERNAME')
    EZID_PASSWORD = 'FILLIN'  #os.environ.get('EZID_PASSWORD')
    EZID_SHOULDER = 'FILLIN'  #os.environ.get('EZID_SHOULDER')
    ECOBROWSER_IP = 'FILLIN'  #os.environ.get('ECOBROWSER_IP')
    ECOBROWSER_USER = 'FILLIN'  # os.environ.get('ECOBROWSER_USER')
    ECOBROWSER_PASSWORD = 'FILLIN'  # os.environ.get('ECOBROWSER_PASSWORD')
    UPLOAD_KEY = 'FILLIN'  #os.environ.get('UPLOAD_KEY')
    BASEDIR = os.path.abspath(os.path.dirname(__file__))
    STATICDIR = os.path.join(basedir, 'ecosynthdata/static')
    VIEWER_LOCATION = 'FILLIN'  #"3dvis.ecosynth.org"  #os.environ.get('VIEWER_LOCATION')

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'data-dev.sqlite')
    WTF_CSRF_ENABLED = False


class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'data-test.sqlite')
    WTF_CSRF_ENABLED = False


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'data.sqlite')

    @classmethod
    def init_app(cls, app):
        Config.init_app(app)


config = {
    'production': ProductionConfig,
    'testing': TestingConfig,
    'default': DevelopmentConfig
}
