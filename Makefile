
help:
	@echo "clean        removes pyc and py~ files"
	@echo "deps         installs dependencies using pip"
	@echo "browser      compiles binary files"
	@echo "clear        removes 

clean:
	find . -name '*.pyc' -delete
	find . -name '*.py~' -delete

deps:
	pip install -r requirements.txt

browser:
	$(MAKE) -C browser_processing/BuildQT
	$(MAKE) -C browser_processing/BuildOct

clear:
	find . -name 'data-dev.sqlite' -delete
	rm -rf uploads/*
	find browser_processing/BuildQT -name 'BuildQuadTree' -delete
	find browser_processing/BuildOct -name 'BuildOctree' -delete	

all:
	mkdir uploads
	$(MAKE) deps
	$(MAKE) browser
