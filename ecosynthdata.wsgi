activate_this = '/var/www/venv/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

import sys
import logging
import os

logging.basicConfig(stream=sys.stderr)
sys.path.insert(0, "/var/www/ecosynthdata/")
sys.path.insert(0, "/var/www/ecosynthdata/ecosynthdata")

from ecosynthdata import create_app

if os.path.exists('/var/www/ecosynthdata/.env'):
    print('Importing environment from .env...')
    for line in open('/var/www/ecosynthdata/.env', 'r'):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]

application = create_app(os.getenv('FLASK_CONFIG') or 'default')
